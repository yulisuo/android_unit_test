package com.yuls.unittest;

import android.app.Service;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.Set;

/**
 * Test1 Service
 */
public class MyService extends Service {

    private static final String TAG = "unit-test";
    private BluetoothAdapter mAdapter;
    private BluetoothA2dp mA2dp;
    private BluetoothHeadset mHfp;
    private final Object mLock = new Object();

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand startId:" + startId);
        getBtInfo();
        return super.onStartCommand(intent, flags, startId);
    }

    private BluetoothProfile.ServiceListener mServiceListener = new BluetoothProfile.ServiceListener() {
        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            Log.i(TAG, "onServiceConnected");
            switch (profile) {
                case BluetoothProfile.A2DP:
                    mA2dp = (BluetoothA2dp) proxy;
                    break;
                case BluetoothProfile.HEADSET:
                    mHfp = (BluetoothHeadset) proxy;
                    break;
            }
            if (mAdapter != null && mA2dp != null && mHfp != null) {
                getBtDeviceInfo();
            }
        }

        @Override
        public void onServiceDisconnected(int profile) {
            switch (profile) {
                case BluetoothProfile.A2DP:
                    mA2dp = null;
                    break;
                case BluetoothProfile.HEADSET:
                    mHfp = null;
                    break;
            }
        }
    };

    private void getBtInfo() {
        Log.i(TAG, "getBtInfo");
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mAdapter.getProfileProxy(this, mServiceListener, BluetoothProfile.A2DP);
        mAdapter.getProfileProxy(this, mServiceListener, BluetoothProfile.HEADSET);
    }


    public void getBtDeviceInfo() {
        Log.i(TAG, "getBtDeviceInfo");
        if (mAdapter == null) {
            Log.i(TAG, "Bt adapter is not initialized.");
            return;
        }
        Set<BluetoothDevice> devices = mAdapter.getBondedDevices();
        if (devices != null && !devices.isEmpty()) {
            for (BluetoothDevice device : devices) {
                Log.i(TAG, "device: " + device.getName() + "(" + device.getAddress() + ")");
                Log.i(TAG, "\ta2dp connect state:" + isConnectA2dp(device));
                Log.i(TAG, "\thfp connect state:" + isConnectHfp(device));
            }
        }
        // TODO stopSelf cause ServiceConnectionLeaked
        //stopSelf();
    }

    private boolean isConnectHfp(BluetoothDevice device) {
        if (mHfp == null) {
            Log.i(TAG, "hfp profile is not initialized.");
            return false;
        }
        return mHfp.getConnectionState(device) == BluetoothAdapter.STATE_CONNECTED;
    }

    private boolean isConnectA2dp(BluetoothDevice device) {
        if (mA2dp == null) {
            Log.i(TAG, "a2dp profile is not initialized.");
            return false;
        }
        return mA2dp.getConnectionState(device) == BluetoothAdapter.STATE_CONNECTED;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
