package com.yuls.unittest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    // test
    EditText et1, et2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn = findViewById(R.id.button_test1);
        btn.setOnClickListener(this);
        btn = findViewById(R.id.button_test2);
        btn.setOnClickListener(this);
        et1 = findViewById(R.id.editText);
        et2 = findViewById(R.id.editText2);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_test1:
                startService(new Intent(this, MyService.class));
                break;
            case R.id.button_test2:
                Intent intent = new Intent(this, Test2Service.class);
                intent.putExtra("num1", Integer.parseInt(et1.getText().toString()));
                intent.putExtra("num2", Integer.parseInt(et2.getText().toString()));
                startService(intent);
                break;
        }
    }

}
