package com.yuls.unittest;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Test2 Service
 */
public class Test2Service extends Service {

    private static final String TAG = "unit-test";

    public Test2Service() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        add(intent.getIntExtra("num1", 0), intent.getIntExtra("num2", 0));
        return super.onStartCommand(intent, flags, startId);
    }

    private void add(int a, int b) {
        Log.i(TAG, "add result is " + (a + b));
    }
}
